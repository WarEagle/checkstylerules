#!groovy

pipeline {
    agent any
    tools {
        maven 'Maven 3.5.3'
        jdk 'JDK 8.162'
    }

    options() {
        buildDiscarder(logRotator(artifactDaysToKeepStr: '', artifactNumToKeepStr: '', daysToKeepStr: '', numToKeepStr: '3'))
        disableConcurrentBuilds()
        timeout(time: 15, unit: 'MINUTES')
    }

    environment {
        MVN_HOME = "${tool 'Maven 3.5.3'}"
        JAVA_HOME = "${tool 'JDK 8.162'}"
        PATH = "${env.MVN_HOME}/bin:${env.JAVA_HOME}/bin:${env.PATH}"
        VERSION = "1.0.${BUILD_NUMBER}-SNAPSHOT"
    }

    stages {
        stage('Checkout') {
            steps {
                checkout scm
            }
        }

        stage ('Initialize') {
            steps {
                sh '''
                    echo "PATH = ${PATH}"
                    echo "M2_HOME = ${M2_HOME}"
                    echo "BUILD_NUMBER = ${BUILD_NUMBER}"
                '''
                //sh "mvn -X build-helper:parse-version versions:set -DpreserveZero -DnewVersion=\\\\${parsedVersion.majorVersion}.\\\\${parsedVersion.minorVersion}.${BUILD_NUMBER}-SNAPSHOT versions:commit -f SourceAnalyzer/pom.xml"
                sh "mvn versions:set -DnewVersion=${VERSION} -f SourceAnalyzer/pom.xml"
            }
        }

        stage('Test') {
            steps {
                sh 'mvn test -fSourceAnalyzer/pom.xml'
            }
        }

        stage('Build') {
            steps {
                sh 'mvn clean package -Dbuild.number=${BUILD_NUMBER} -fSourceAnalyzer/pom.xml'
            }
        }

        stage('Distribute Snapshot') {
            when {
        		    branch 'develop'
        		 }
            steps {
                configFileProvider(
                    [configFile(fileId: 'maven-global-settings.xml', variable: 'MAVEN_SETTINGS')]) {
                    sh 'mvn -s $MAVEN_SETTINGS clean deploy -fSourceAnalyzer/pom.xml'
                }
            }
        }
    }

    post {
        success {
            junit 'SourceAnalyzer/target/surefire-reports/*.xml'
        }

        failure {
            script {
                recipients = 'jenkins@tkunkel.de'
                echo 'Notification sent to: ' + recipients

                emailext (
                        to: recipients,
                        subject: "Jenkins Failed: Job '${env.JOB_NAME} [${env.BUILD_NUMBER}]'",
                        body: """<p>Job '${env.JOB_NAME} [${env.BUILD_NUMBER}]':</p>""",
                )
            }
        }
    }
}
