# Open Tasks

## Checks
* check switch-statements for completeness

## Organizational
* create changelog
* write user documentation
* create demo project where lib is used as test
* publish lib somehow