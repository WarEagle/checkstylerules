# Functional Source Analyser

## Ideas
This project adds the possibility to the buildprocess to check the code for functional requirements.

### Checker
#### Using Switch for Enums
Using the switch statement for checking enums has the following advantages:
* type safety is ensured
* abstraction from the enum value to a meaning, normally multiple enum values are "valid" for the use case
* when extending the enum the developer has do decide how this code should react on the new enum value and it cannot be overseen ([see SwitchEnumComplete](#switchEnumComplete))
#### NoEnumConditional
##### Concept
Compares of enums by using == is not desired, the switch statement should be used.
##### Reasoning
[Why switch?](#Using-Switch-for-Enums)
##### Example
```java
package de.tkunkel.oss.functional.source.analyzer.noenumconditional;

enum MyEnum {
    A, B;
}

public class IfSimple {
    public void test1(MyEnum variable) {
        if (variable == MyEnum.A) { // <---- marked as problem
        }
    }
}
```
##### Usage

#### NoEnumEquals
##### Concept
* do not use equals to compare enums
* if you have to compare two objects with each other use == to ensure type savety
* if you want to compare against a specific enum value use switch
##### Reasoning
[Why switch?](#Using-Switch-for-Enums)
##### Example
```java
package de.tkunkel.oss.functional.source.analyzer.noenumequals;

enum MyEnum {
    A, B;
}

public class SimpleEquals {
    public boolean test1(MyEnum variable) {
        boolean rc = variable.equals(MyEnum.A);// <---- marked as problem
        return rc;
    }
}
```

#### SwitchEnumComplete
##### Concept
##### Reasoning
[Why switch?](#Using-Switch-for-Enums)
##### Usage
##### Example
```java
package de.tkunkel.oss.functional.source.analyzer.switchenumcomplete;

enum MyEnum {
    A, B;
}

public class SwitchSimpleMissing {
    public void test1(MyEnum variable) {
        switch (variable) {// <---- marked as problem, B is missing
            case A:
                System.out.println("A");
                break;
            default:
                System.out.println("None");
        }
    }
}
```
