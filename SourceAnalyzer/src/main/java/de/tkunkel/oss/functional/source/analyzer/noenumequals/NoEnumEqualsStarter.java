package de.tkunkel.oss.functional.source.analyzer.noenumequals;

import de.tkunkel.oss.functional.source.analyzer.AbstractChecker;
import de.tkunkel.oss.functional.source.analyzer.ManualStarter;
import de.tkunkel.oss.functional.source.analyzer.model.FunctionalProblemFinding;
import org.apache.logging.log4j.LogManager;

import java.io.IOException;
import java.nio.file.Path;
import java.util.Collection;
import java.util.function.Predicate;

@SuppressWarnings({"squid:CommentedOutCodeLine"})
public class NoEnumEqualsStarter extends ManualStarter {

    private NoEnumEqualsStarter() {
        logger = LogManager.getLogger(NoEnumEqualsStarter.class);
    }

    @Override
    protected AbstractChecker getChecker(String absolutePath, String baseDir) throws IOException {
        AbstractChecker abstractChecker = new NoEnumEqualsChecker(baseDir);
        abstractChecker.setProjectDirToScan(absolutePath);
        return abstractChecker;
    }

    public static void main(String[] args) throws IOException, InterruptedException {
        if (args.length < 1) {
            System.err.println("Please provide directory to scan.");
            System.exit(-1);
        }
        String baseDir = args[0];
        ManualStarter starter = new NoEnumEqualsStarter();
        Collection<FunctionalProblemFinding> results = starter.startForDirectory(baseDir, getProjectFilter(), getFileFilter());
        starter.outputToLogToConsole(results);
        starter.outputToLogToFile(results);
    }

    @SuppressWarnings({"squid:S1602"})
    private static Predicate<Path> getProjectFilter() {
        return path -> {
//            return path.toString().contains("org.jfreechart");
            return true;
        };
    }

    @SuppressWarnings({"squid:S1602"})
    private static Predicate<Path> getFileFilter() {
        return path -> {
//            return path.toString().contains("AbstractCategoryItemRenderer");
            return true;
        };
    }


}
