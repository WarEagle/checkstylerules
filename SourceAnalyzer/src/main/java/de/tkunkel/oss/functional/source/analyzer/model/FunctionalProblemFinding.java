package de.tkunkel.oss.functional.source.analyzer.model;

import java.util.Objects;

/**
 * Class to collect the results of a test result finding. Contains the position and code where the problem occurred.
 */
public class FunctionalProblemFinding implements Comparable<FunctionalProblemFinding> {
    private final String fileName;
    private final String packageName;
    private final String code;
    private final long lineNumber;
    private final long columnNumber;

    /**
     * Creates new instance
     *
     * @param fileName
     * @param packageName
     * @param lineNumber
     * @param columnNumber
     * @param code
     */
    public FunctionalProblemFinding(String fileName, String packageName, long lineNumber, long columnNumber, String code) {
        this.fileName = fileName;
        this.packageName = packageName;
        this.lineNumber = lineNumber;
        this.columnNumber = columnNumber;
        this.code = code;
    }

    /**
     * Gets the filename the problem occurred
     *
     * @return
     */
    public String getFileName() {
        return fileName;
    }

    /**
     * Gets the filename the problem occurred without the file ending.<br>
     * often used as classname
     *
     * @return
     */
    public String getFileNameWithoutEnding() {
        int last = fileName.lastIndexOf('.');
        if (last != -1) {
            return fileName.substring(0, last);
        } else {
            return fileName;
        }
    }

    /**
     * Gets the line number the problem occurred
     *
     * @return
     */
    public long getLineNumber() {
        return lineNumber;
    }

    /**
     * Gets the column the problem occurred
     *
     * @return
     */
    public long getColumnNumber() {
        return columnNumber;
    }

    @Override
    public String toString() {
        return "FunctionalProblemFinding{" +
                "fileName='" + fileName + '\'' +
                ", packageName=" + packageName +
                ", lineNumber=" + lineNumber +
                ", columnNumber=" + columnNumber +
                ", code=" + code +
                '}';
    }

    /**
     * Gets the information formatted for display
     *
     * @return
     */
    public String toDisplayString() {
        return "" + packageName + "(" + fileName + ":" + lineNumber + ")\n" + code;
    }

    /**
     * Gets the code that causes the problem
     *
     * @return
     */
    public String getCode() {
        return code;
    }

    /**
     * Gets the packagename of the file the problem occurred
     *
     * @return
     */
    public String getPackageName() {
        return packageName;
    }


    @Override
    public int compareTo(FunctionalProblemFinding o) {
        String a = o.packageName + o.fileName + o.lineNumber + o.columnNumber;
        String b = this.packageName + this.fileName + this.lineNumber + this.columnNumber;
        return b.compareTo(a);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        FunctionalProblemFinding that = (FunctionalProblemFinding) o;
        return lineNumber == that.lineNumber &&
                columnNumber == that.columnNumber &&
                Objects.equals(fileName, that.fileName) &&
                Objects.equals(packageName, that.packageName) &&
                Objects.equals(code, that.code);
    }

    @Override
    public int hashCode() {
        return Objects.hash(fileName, packageName, code, lineNumber, columnNumber);
    }
}
