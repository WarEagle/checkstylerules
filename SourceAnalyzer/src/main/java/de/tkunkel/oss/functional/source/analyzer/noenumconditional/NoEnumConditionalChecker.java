package de.tkunkel.oss.functional.source.analyzer.noenumconditional;

import com.github.javaparser.ast.CompilationUnit;
import com.github.javaparser.ast.PackageDeclaration;
import com.github.javaparser.ast.visitor.ModifierVisitor;
import de.tkunkel.oss.functional.source.analyzer.AbstractChecker;
import de.tkunkel.oss.functional.source.analyzer.model.FunctionalProblemFinding;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class NoEnumConditionalChecker extends AbstractChecker {
    private static final Logger LOGGER = LogManager.getLogger(NoEnumConditionalChecker.class);

    public NoEnumConditionalChecker(AbstractChecker copy) {
        super(copy);
    }

    public NoEnumConditionalChecker(String baseDir) throws IOException {
        super(baseDir);
        LOGGER.debug("init");
    }

    @Override
    public List<FunctionalProblemFinding> checkCompilationUnit(CompilationUnit cu, String fileName) {
        List<FunctionalProblemFinding> rc = new ArrayList<>();

        String[] packageName = new String[1];
        packageName[0] = "";
        if (cu.getPackageDeclaration().isPresent()) {
            packageName[0] = cu.getPackageDeclaration().orElseGet(PackageDeclaration::new).getName().toString();
        }

        cu.accept(getVisitor(fileName, rc, packageName), null);

        return rc;
    }

    private ModifierVisitor<Void> getVisitor(String fileName, List<FunctionalProblemFinding> rc, String[] packageName) {
        return new VoidModifierVisitor(fileName, packageName, rc, this);
    }



}
