package de.tkunkel.oss.functional.source.analyzer.helper;

import com.github.javaparser.ast.expr.Expression;
import com.github.javaparser.ast.expr.FieldAccessExpr;
import com.github.javaparser.ast.expr.NameExpr;
import com.github.javaparser.ast.expr.ThisExpr;
import com.github.javaparser.ast.stmt.SwitchEntryStmt;
import com.github.javaparser.ast.stmt.SwitchStmt;
import com.github.javaparser.resolution.UnsolvedSymbolException;
import com.github.javaparser.resolution.declarations.ResolvedFieldDeclaration;
import com.github.javaparser.resolution.declarations.ResolvedValueDeclaration;
import com.github.javaparser.resolution.types.ResolvedType;
import com.github.javaparser.symbolsolver.javaparsermodel.JavaParserFacade;
import com.github.javaparser.symbolsolver.javaparsermodel.declarations.JavaParserEnumDeclaration;
import com.github.javaparser.symbolsolver.javaparsermodel.declarations.JavaParserFieldDeclaration;
import com.github.javaparser.symbolsolver.model.resolution.SymbolReference;
import com.github.javaparser.symbolsolver.model.resolution.TypeSolver;
import com.github.javaparser.symbolsolver.resolution.typesolvers.ReflectionTypeSolver;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;

public class TypeHelper {

    private static final Logger LOGGER = LogManager.getLogger(TypeHelper.class);
    private final String DEFAULT = "default";
    private final TypeSolver combinedTypeSolver;

    public TypeHelper(TypeSolver combinedTypeSolver) {
        this.combinedTypeSolver = combinedTypeSolver;
    }

    public ResolvedType getType(Expression expression) {
        JavaParserFacade javaParserFacade = JavaParserFacade.get(combinedTypeSolver);
        ResolvedType resolvedType = javaParserFacade.getType(expression);

        return resolvedType;
    }

    public boolean isEnum(Expression expression) {
        if (expression.isFieldAccessExpr()) {
            JavaParserFacade javaParserFacade = JavaParserFacade.get(combinedTypeSolver);
            try {
                ResolvedType resolvedType = javaParserFacade.getType(expression);
                if (resolvedType.isReferenceType()) {
                    return resolvedType.asReferenceType().getTypeDeclaration().isEnum();
                }
            } catch (RuntimeException e) {
                LOGGER.warn(e);
                return false;
            }
            return false;
        } else {
            return false;
        }
    }

    public Set<String> getEntriesOfEnum(final SwitchStmt switchStmt) {
        Expression expression = switchStmt.getSelector();

        Set<String> entriesOfEnum = new HashSet<>();

        if (expression instanceof ThisExpr) {
            LOGGER.debug("Found ThisExpression, using label to identify enum content");
            entriesOfEnum = getEntriesOfEnumByLabel(switchStmt);
            return entriesOfEnum;
        }


        SymbolReference<? extends ResolvedValueDeclaration> ref = null;
        try {
            ref = JavaParserFacade.get(new ReflectionTypeSolver()).solve(expression);
        } catch (UnsolvedSymbolException | IllegalArgumentException e) {
            LOGGER.debug(e);
        }
        if (ref == null || !ref.isSolved()) {
            return entriesOfEnum;
        }

        fillEntriesOfEnum(expression, entriesOfEnum);
        return entriesOfEnum;
    }

    private void fillEntriesOfEnum(Expression expression, Set<String> entriesOfEnum) {
        JavaParserFacade javaParserFacade = JavaParserFacade.get(combinedTypeSolver);
        ResolvedType type = null;
        try {
            type = javaParserFacade.getType(expression);
        } catch (UnsolvedSymbolException | IllegalArgumentException e) {
            LOGGER.debug(e);
        }
        if (type != null && type.isReferenceType() && type.asReferenceType().getTypeDeclaration() instanceof JavaParserEnumDeclaration) {
            List<ResolvedFieldDeclaration> allFields = type.asReferenceType().getTypeDeclaration().getAllFields();
            for (ResolvedFieldDeclaration field : allFields) {
                String fieldName = field.getName();
                String switchField = expression.toString();
                JavaParserFieldDeclaration field1 = (JavaParserFieldDeclaration) field;
                if (!fieldName.equalsIgnoreCase(switchField) && field1.getWrappedNode() == null) {
                    entriesOfEnum.add(field.getName());
                }
            }
        }
    }

    /**
     * If the variable passed to the switch statement isn't usable for determing the contents of the enum, one of the labels is used.
     * They should have the same type as the variable, so the results are the same, it's just indirect.
     *
     * @param switchStmt
     * @return
     */
    private Set<String> getEntriesOfEnumByLabel(final SwitchStmt switchStmt) {
        Set<String> entriesOfEnum = new HashSet<>();

        for (SwitchEntryStmt switchEntryStmt : switchStmt.getEntries()) {
            Optional<Expression> stmtLabel = switchEntryStmt.getLabel();
            if (!stmtLabel.isPresent()) {
                continue;
            }

            String label = DEFAULT;
            Expression expression = null;
            if (stmtLabel.get().isFieldAccessExpr()) {
                label = ((FieldAccessExpr) stmtLabel.get()).getName().asString();
            }
            if (stmtLabel.get().isNameExpr()) {
                expression = stmtLabel.get();
                label = ((NameExpr) stmtLabel.get()).getName().asString();
            }
            if (!(DEFAULT.equals(label)) && expression != null) {
                JavaParserFacade javaParserFacade = JavaParserFacade.get(combinedTypeSolver);
                ResolvedType type = null;
                try {
                    type = javaParserFacade.getType(expression);
                } catch (UnsolvedSymbolException | IllegalArgumentException e) {
                    LOGGER.debug(e);
                }
                if (type != null && type.isReferenceType() && type.asReferenceType().getTypeDeclaration() instanceof JavaParserEnumDeclaration) {
                    List<ResolvedFieldDeclaration> allFields = type.asReferenceType().getTypeDeclaration().getAllFields();
                    for (ResolvedFieldDeclaration field : allFields) {
                        if (field.getType().isReferenceType()) {
                            String fieldName = field.getName();
                            entriesOfEnum.add(fieldName);
                        }
                    }
                }
            }
        }
        return entriesOfEnum;
    }

    public Set<String> getLabelsOfSwitch(SwitchStmt switchStmt) {
        Set<String> entriesOfSwitch = new HashSet<>();

        for (SwitchEntryStmt switchEntryStmt : switchStmt.getEntries()) {
            Optional<Expression> stmtLabel = switchEntryStmt.getLabel();
            if (!stmtLabel.isPresent()) {
                continue;
            }

            String label = DEFAULT;
            if (stmtLabel.get().isFieldAccessExpr()) {
                label = ((FieldAccessExpr) stmtLabel.get()).getName().asString();
            }
            if (stmtLabel.get().isNameExpr()) {
                label = ((NameExpr) stmtLabel.get()).getName().asString();
            }
            if (!(DEFAULT.equals(label))) {
                entriesOfSwitch.add(label);
            }
        }

        return entriesOfSwitch;
    }
}
