package de.tkunkel.oss.functional.source.analyzer.output;

import de.tkunkel.oss.functional.source.analyzer.model.FunctionalProblemFinding;
import j2html.tags.ContainerTag;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static j2html.TagCreator.body;
import static j2html.TagCreator.br;
import static j2html.TagCreator.div;
import static j2html.TagCreator.h2;
import static j2html.TagCreator.span;

/**
 * Class for exporting tha analysis results as HTML
 */
public class HtmlWriter {
    private static final Logger LOGGER = LogManager.getLogger(HtmlWriter.class);

    private HtmlWriter() {
    }

    /**
     * Export the given results to the file result.html in the current folder.
     * @param results
     */
    public static void outputToFile(Collection<FunctionalProblemFinding> results) {
        ContainerTag body = body(
                h2("Found " + results.size() + " problems.")
        );


        try (BufferedWriter br = new BufferedWriter(new FileWriter("result.html"))) {
            Map<String, List<FunctionalProblemFinding>> map = results.stream().collect(Collectors.groupingBy(o -> o.getPackageName() + '.' + o.getFileNameWithoutEnding()));
            map.values().forEach(enumInIfFindings -> {
                ContainerTag tag = div();

                tag.with(
                        generateFileInfo(enumInIfFindings.get(0))
                ).withStyle("margin-bottom: 2em;background-color:#F0F0F0;");

                for (FunctionalProblemFinding functionalProblemFinding : enumInIfFindings) {
                    tag.with(
                            generateCodeBlock(functionalProblemFinding)
                    );
                }
                body.with(tag);

            });

            br.write(body.render());
            br.flush();
        } catch (IOException e) {
            LOGGER.error("", e);
        }

    }

    private static ContainerTag generateFileInfo(FunctionalProblemFinding r) {
        ContainerTag tag = div();
        tag.with(
                span(r.getPackageName() + "."),
                span(r.getFileNameWithoutEnding()).withStyle("color:red;"),
                span(":"),
                br()
        );
        return tag;
    }

    private static ContainerTag generateCodeBlock(FunctionalProblemFinding r) {
        ContainerTag tag = div();
        tag.with(
                span(r.getLineNumber() + ":").withStyle("margin-right:1em;"),
                span(r.getCode()).withStyle("margin-left:1em")
        ).withStyle("margin-bottom: 0.5em;");
        return tag;
    }

}
