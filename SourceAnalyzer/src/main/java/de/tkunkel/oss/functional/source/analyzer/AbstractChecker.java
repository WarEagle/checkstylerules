package de.tkunkel.oss.functional.source.analyzer;

import com.github.javaparser.ast.CompilationUnit;
import com.github.javaparser.symbolsolver.resolution.typesolvers.CombinedTypeSolver;
import com.github.javaparser.symbolsolver.resolution.typesolvers.JarTypeSolver;
import com.github.javaparser.symbolsolver.resolution.typesolvers.JavaParserTypeSolver;
import com.github.javaparser.utils.CodeGenerationUtils;
import com.github.javaparser.utils.SourceRoot;
import de.tkunkel.oss.functional.source.analyzer.helper.TypeHelper;
import de.tkunkel.oss.functional.source.analyzer.model.FunctionalProblemFinding;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Stream;

/**
 * Collection of common methods of all checkers
 */
public abstract class AbstractChecker {

    private static final Logger LOGGER = LogManager.getLogger(AbstractChecker.class);

    private CombinedTypeSolver combinedTypeSolver;
    private TypeHelper typeHelper;

    private final CombinedTypeSolver combinedTypeSolverRaw;
    private Set<String> possibleSubdirectories = new HashSet<>();

    protected AbstractChecker(String directoryWithDependencies) throws IOException {

        possibleSubdirectories.add("\\src");
        possibleSubdirectories.add("\\src\\java");
        possibleSubdirectories.add("\\src\\main\\java");


        combinedTypeSolverRaw = new CombinedTypeSolver();
        addClasspath(combinedTypeSolverRaw);
        addAdditionalSources(directoryWithDependencies);
    }

    protected AbstractChecker(AbstractChecker copy) {
        this.combinedTypeSolver = copy.combinedTypeSolver;
        this.combinedTypeSolverRaw = copy.combinedTypeSolverRaw;
        this.typeHelper = copy.typeHelper;
        this.possibleSubdirectories = copy.possibleSubdirectories;
    }

    public TypeHelper getTypeHelper() {
        return typeHelper;
    }

    public CombinedTypeSolver getCombinedTypeSolver() {
        return combinedTypeSolver;
    }

    public void setProjectDirToScan(String projectDirToScan) {
        LOGGER.debug("Using {} as projectDirToScan.", projectDirToScan);

        SourceRoot root = new SourceRoot(CodeGenerationUtils.classLoaderRoot(AbstractChecker.class).resolve(projectDirToScan));
        combinedTypeSolver = new CombinedTypeSolver();
        combinedTypeSolver.add(combinedTypeSolverRaw);
        combinedTypeSolver.add(new JavaParserTypeSolver(root.getRoot().toFile()));
        typeHelper = new TypeHelper(combinedTypeSolver);
    }

    @Override
    public String toString() {
        return getClass().getSimpleName();
    }

    private void addClasspath(CombinedTypeSolver givenCombinedTypeSolver) {
        String classPath = System.getProperty("java.class.path");
        for (String file : classPath.split(File.pathSeparator)) {
            if (file.toLowerCase().endsWith("jar")) {
                try {
                    LOGGER.debug("Adding '{}' as JarTypeSolver", file);
                    givenCombinedTypeSolver.add(new JarTypeSolver(file));
                } catch (IOException e) {
                    LOGGER.error(e);
                }
            }
        }
    }

    private void addAdditionalSources(String directoryWithDependencies) throws IOException {
        if (directoryWithDependencies == null) {
            return;
        }

        try (Stream<Path> files = Files.list(Paths.get(directoryWithDependencies))) {
            files.forEach(file -> {
                if (file.toFile().isDirectory() && !file.toFile().getName().startsWith(".")) {
                    addSource(file, directoryWithDependencies);
                }
            });
        }
    }

    private void addSource(Path file, String baseDir) {
        String name = file.toFile().getName();
        for (String subdirectory : possibleSubdirectories) {
            File dir = new File(baseDir + File.separator + name + File.separator + subdirectory).getAbsoluteFile();
            if (dir.exists()) {
                LOGGER.debug("Adding {} to JavaParserTypeSolver ", dir);
                combinedTypeSolverRaw.add(new JavaParserTypeSolver(dir));
            }
        }
    }

    public abstract List<FunctionalProblemFinding> checkCompilationUnit(CompilationUnit cu, String fileName);

}
