package de.tkunkel.oss.functional.source.analyzer.noenumequals;

import com.github.javaparser.Position;
import com.github.javaparser.ast.CompilationUnit;
import com.github.javaparser.ast.PackageDeclaration;
import com.github.javaparser.ast.expr.Expression;
import com.github.javaparser.ast.expr.MethodCallExpr;
import com.github.javaparser.ast.visitor.ModifierVisitor;
import com.github.javaparser.ast.visitor.Visitable;
import de.tkunkel.oss.functional.source.analyzer.AbstractChecker;
import de.tkunkel.oss.functional.source.analyzer.model.FunctionalProblemFinding;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicBoolean;

public class NoEnumEqualsChecker extends AbstractChecker {
    private static final Logger LOGGER = LogManager.getLogger(NoEnumEqualsChecker.class);
    private static final String STRING_EQUALS = "equals";

    public NoEnumEqualsChecker(AbstractChecker copy) {
        super(copy);
    }

    public NoEnumEqualsChecker(String baseDir) throws IOException {
        super(baseDir);

        LOGGER.debug("init");
    }

    @Override
    public List<FunctionalProblemFinding> checkCompilationUnit(CompilationUnit cu, String fileName) {
        List<FunctionalProblemFinding> rc = new ArrayList<>();

        String[] packageName = new String[1];
        packageName[0] = "";
        if (cu.getPackageDeclaration().isPresent()) {
            packageName[0] = cu.getPackageDeclaration().orElseGet(PackageDeclaration::new).getName().toString();
        }

        cu.accept(new ModifierVisitor<Void>() {

            @Override
            public Visitable visit(final MethodCallExpr methodCallExpr, final Void arg) {

                if (!STRING_EQUALS.equals(methodCallExpr.getName().asString())) {
                    return super.visit(methodCallExpr, arg);
                }
                LOGGER.debug("checking method '{}'", methodCallExpr.getName().asString());

                boolean isParameterEnum = isAnyParameterEnum(methodCallExpr);
                boolean isScopeEnum = isScopeAEnum(methodCallExpr);
                LOGGER.debug("isParameterEnum {}", isParameterEnum);
                LOGGER.debug("isScopeEnum {}", isScopeEnum);

                if (isParameterEnum || isScopeEnum) {
                    Position pos = methodCallExpr.getBegin().orElseGet(() -> new Position(-1, -1));
                    String code = methodCallExpr.toString();
                    FunctionalProblemFinding finding = new FunctionalProblemFinding(fileName, packageName[0], pos.line, pos.column, code);
                    rc.add(finding);
                }


                return super.visit(methodCallExpr, arg);
            }

        }, null);

        return rc;
    }

    private boolean isAnyParameterEnum(MethodCallExpr methodCallExpr) {
        AtomicBoolean rc = new AtomicBoolean(false);
        methodCallExpr.getArguments().forEach(expression -> rc.set(rc.get() || getTypeHelper().isEnum(expression)));
        return rc.get();
    }

    private boolean isScopeAEnum(MethodCallExpr methodCallExpr) {
        Optional<Expression> scope = methodCallExpr.getScope();
        if (!scope.isPresent()) {
            return false;
        }
        return getTypeHelper().isEnum(scope.get());
    }

}
