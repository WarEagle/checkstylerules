package de.tkunkel.oss.functional.source.analyzer.switchenumcomplete;

import com.github.javaparser.Position;
import com.github.javaparser.ast.CompilationUnit;
import com.github.javaparser.ast.PackageDeclaration;
import com.github.javaparser.ast.expr.Expression;
import com.github.javaparser.ast.stmt.SwitchStmt;
import com.github.javaparser.ast.visitor.ModifierVisitor;
import com.github.javaparser.ast.visitor.Visitable;
import com.github.javaparser.resolution.types.ResolvedType;
import com.github.javaparser.symbolsolver.javaparsermodel.JavaParserFacade;
import com.github.javaparser.symbolsolver.model.typesystem.ReferenceTypeImpl;
import de.tkunkel.oss.functional.source.analyzer.AbstractChecker;
import de.tkunkel.oss.functional.source.analyzer.model.FunctionalProblemFinding;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.atomic.AtomicBoolean;

public class SwitchEnumCompleteChecker extends AbstractChecker {
    private static final Logger LOGGER = LogManager.getLogger(SwitchEnumCompleteChecker.class);

    public SwitchEnumCompleteChecker(AbstractChecker copy) {
        super(copy);
    }


    public SwitchEnumCompleteChecker(String baseDir) throws IOException {
        super(baseDir);
        LOGGER.debug("init");
    }

    @Override
    public List<FunctionalProblemFinding> checkCompilationUnit(CompilationUnit cu, String fileName) {
        List<FunctionalProblemFinding> rc = new ArrayList<>();

        String[] packageName = new String[1];
        packageName[0] = "";
        if (cu.getPackageDeclaration().isPresent()) {
            packageName[0] = cu.getPackageDeclaration().orElseGet(PackageDeclaration::new).getName().toString();
        }

        cu.accept(new ModifierVisitor<Void>() {

            @Override
            public Visitable visit(final SwitchStmt switchStmt, final Void arg) {

                boolean isEnum = isSelectorEnum(switchStmt.getSelector());

                if (!isEnum) {
                    return super.visit(switchStmt, arg);
                }

                Set<String> entriesOfEnum = getTypeHelper().getEntriesOfEnum(switchStmt);
                Set<String> entriesOfSwitch = getTypeHelper().getLabelsOfSwitch(switchStmt);

                Set<String> missingLabels = findMissingLabels(fileName, entriesOfEnum, entriesOfSwitch);
                Position pos = switchStmt.getBegin().orElseGet(() -> new Position(-1, -1));

                LOGGER.debug("Missing {}", missingLabels);

                for (String missingLabel : missingLabels) {
                    String msg = "Missing " + missingLabel + "\n";
                    FunctionalProblemFinding finding = new FunctionalProblemFinding(fileName, packageName[0], pos.line, pos.column, msg + switchStmt.toString());
                    rc.add(finding);
                }

                return super.visit(switchStmt, arg);
            }
        }, null);

        return rc;
    }

    private boolean isSelectorEnum(Expression selector) {
        if (selector.isMethodCallExpr()) {
            return false;
        }

        JavaParserFacade javaParserFacade = JavaParserFacade.get(getCombinedTypeSolver());
        ResolvedType type = javaParserFacade.getType(selector);

        AtomicBoolean rc = new AtomicBoolean(false);
        if (type.isReferenceType()) {
            ((ReferenceTypeImpl) type).getAllClassesAncestors().forEach(resolvedReferenceType -> rc.set(rc.get() || "Enum".equals(resolvedReferenceType.getTypeDeclaration().getClassName())));
        }

        return rc.get();
    }

    private Set<String> findMissingLabels(String fileName, Set<String> entriesOfEnum, Set<String> entriesOfSwitch) {
        LOGGER.debug("Found in enum   {}", entriesOfEnum);
        LOGGER.debug("Found in switch {}", entriesOfSwitch);

        if (entriesOfEnum.size() < entriesOfSwitch.size()) {
            LOGGER.warn("Cannot parse switch in {}, enum={}, switch={}", fileName, entriesOfEnum, entriesOfSwitch);
        }
        Set<String> missing = new HashSet<>();

        if (entriesOfEnum.isEmpty()) {
            return missing;
        }

        for (String entryOfEnum : entriesOfEnum) {
            if (!entriesOfSwitch.contains(entryOfEnum)) {
                missing.add(entryOfEnum);
            }
        }

        return missing;
    }

}
