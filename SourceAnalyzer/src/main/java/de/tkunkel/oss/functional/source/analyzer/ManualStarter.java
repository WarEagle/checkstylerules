package de.tkunkel.oss.functional.source.analyzer;

import com.github.javaparser.ast.CompilationUnit;
import com.github.javaparser.symbolsolver.javaparsermodel.JavaParserFacade;
import com.github.javaparser.utils.CodeGenerationUtils;
import com.github.javaparser.utils.SourceRoot;
import de.tkunkel.oss.functional.source.analyzer.model.FunctionalProblemFinding;
import de.tkunkel.oss.functional.source.analyzer.noenumconditional.NoEnumConditionalChecker;
import de.tkunkel.oss.functional.source.analyzer.output.HtmlWriter;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.ForkJoinPool;
import java.util.concurrent.TimeUnit;
import java.util.function.Predicate;
import java.util.stream.Stream;

public abstract class ManualStarter {
    protected Logger logger;

    protected ManualStarter() {
        logger = LogManager.getLogger(ManualStarter.class);
    }

    public Collection<FunctionalProblemFinding> startForDirectory(String baseDir, Predicate<Path> projectFilter, Predicate<Path> fileFilter) throws IOException, InterruptedException {
        long start = System.currentTimeMillis();

        Path classLoaderRoot = CodeGenerationUtils.classLoaderRoot(NoEnumConditionalChecker.class);

        Set<FunctionalProblemFinding> results = new HashSet<>();

        ForkJoinPool forkJoinPool = new ForkJoinPool(5);
        try (Stream<Path> files = Files.list(Paths.get(baseDir))) {
            files
                    .filter(file -> file.toFile().isDirectory())
                    .filter(file -> projectFilter != null && projectFilter.test(file))
                    .forEach(file -> process(baseDir, fileFilter, classLoaderRoot, results, forkJoinPool, file));
        }
        forkJoinPool.shutdown();
        forkJoinPool.awaitTermination(60, TimeUnit.MINUTES);

        logger.info("Done in {}ms", System.currentTimeMillis() - start);

        return results;
    }

    private void process(String baseDir, Predicate<Path> fileFilter, Path classLoaderRoot, Collection<FunctionalProblemFinding> results, ForkJoinPool forkJoinPool, Path file) {
        forkJoinPool.submit(() -> {
            logger.info("Scanning module {}", file.getFileName());
            AbstractChecker abstractChecker = null;
            try {
                abstractChecker = getChecker(file.toFile().getAbsolutePath(), baseDir);
            } catch (IOException e) {
                logger.error(e);
            }

            SourceRoot root = new SourceRoot(classLoaderRoot.resolve(file.toFile().getAbsolutePath()));
            Path path = root.getRoot();
            scanDirectory(results, abstractChecker, root, path, fileFilter);
            JavaParserFacade.clearInstances();
        });
    }

    protected abstract AbstractChecker getChecker(String absolutePath, String baseDir) throws IOException;

    public void outputToLogToFile(Collection<FunctionalProblemFinding> results) {
        HtmlWriter.outputToFile(results);
    }

    public void outputToLogToConsole(Collection<FunctionalProblemFinding> results) {
        logger.warn("Found {} problems.", results.size());

        StringBuilder sb = new StringBuilder();
        sb.append('\n');
        results.forEach(r -> {
            sb.append(r.toDisplayString());
            sb.append('\n');
            sb.append('\n');
        });
        logger.error(sb.toString());
    }

    private void scanDirectory(Collection<FunctionalProblemFinding> results, AbstractChecker abstractChecker, SourceRoot root, Path path, Predicate<Path> fileFilter) {
        try (Stream<Path> files = Files.list(path)) {
            files
                    .filter(isRelevantFileOrIsDirectory(fileFilter))
                    .forEach(file -> processFileOrDirectory(results, abstractChecker, root, file, fileFilter));
        } catch (IOException e) {
            logger.error(e);
        }
    }

    private void processFileOrDirectory(Collection<FunctionalProblemFinding> results, AbstractChecker abstractChecker, SourceRoot root, Path file, Predicate<Path> fileFilter) {
        if (file.toFile().isDirectory()) {
            scanDirectory(results, abstractChecker, root, file, fileFilter);
        } else {
            logger.debug("scanning {}", file);
            String fileName = file.getFileName().toString();
            String path = getPathWithoutRoot(file, root);
            CompilationUnit cu = root.parse(path, fileName);
            results.addAll(abstractChecker.checkCompilationUnit(cu, fileName));
        }
    }

    private String getPathWithoutRoot(Path file, SourceRoot root) {
        return file.getParent().toString().substring(root.getRoot().toFile().getAbsolutePath().length() + 1);
    }

    private Predicate<Path> isRelevantFileOrIsDirectory(Predicate<Path> fileFilter) {
        return file -> {
            // not using intermediate booleans due to performance
            if (file.toFile().isDirectory()) {
                return true;
            }
            if (file.toFile().getName().endsWith("Test.java")) {
                return false;
            }
            if (!file.toFile().getName().endsWith("java")) {
                return false;
            }
            if (!fileFilter.test(file)) {
                return false;
            }
            return true;
        };
    }

}
