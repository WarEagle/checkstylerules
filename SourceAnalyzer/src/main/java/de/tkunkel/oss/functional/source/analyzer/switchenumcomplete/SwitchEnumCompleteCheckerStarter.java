package de.tkunkel.oss.functional.source.analyzer.switchenumcomplete;

import de.tkunkel.oss.functional.source.analyzer.AbstractChecker;
import de.tkunkel.oss.functional.source.analyzer.ManualStarter;
import de.tkunkel.oss.functional.source.analyzer.model.FunctionalProblemFinding;
import org.apache.logging.log4j.LogManager;

import java.io.IOException;
import java.nio.file.Path;
import java.util.Collection;
import java.util.function.Predicate;

@SuppressWarnings({"squid:CommentedOutCodeLine"})
public class SwitchEnumCompleteCheckerStarter extends ManualStarter {

    private SwitchEnumCompleteCheckerStarter() {
        logger = LogManager.getLogger(SwitchEnumCompleteCheckerStarter.class);
    }

    @Override
    protected AbstractChecker getChecker(String absolutePath, String baseDir) throws IOException {
        AbstractChecker abstractChecker = new SwitchEnumCompleteChecker(baseDir);
        abstractChecker.setProjectDirToScan(absolutePath);
        return abstractChecker;
    }

    public static void main(String[] args) throws IOException, InterruptedException {
        if (args.length < 1) {
            System.err.println("Please provide directory to scan.");
            System.exit(-1);
        }
        String baseDir = args[0];
        ManualStarter starter = new SwitchEnumCompleteCheckerStarter();
        Collection<FunctionalProblemFinding> results = starter.startForDirectory(baseDir, getProjectFilter(), getFileFilter());
        starter.outputToLogToConsole(results);
        starter.outputToLogToFile(results);
    }

    @SuppressWarnings({"squid:S1602"})
    private static Predicate<Path> getProjectFilter() {
        return path -> {
            return true;
//            return path.toString().contains("ignore.me");
        };
    }

    @SuppressWarnings({"squid:S1602"})
    private static Predicate<Path> getFileFilter() {
        return path -> {
            return true;
//            return path.toString().contains("TimeValueTest");
        };
    }

}
