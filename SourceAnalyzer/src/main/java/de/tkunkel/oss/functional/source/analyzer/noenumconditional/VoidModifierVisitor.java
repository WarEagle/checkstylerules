package de.tkunkel.oss.functional.source.analyzer.noenumconditional;

import com.github.javaparser.Position;
import com.github.javaparser.ast.Node;
import com.github.javaparser.ast.expr.BinaryExpr;
import com.github.javaparser.ast.expr.EnclosedExpr;
import com.github.javaparser.ast.expr.Expression;
import com.github.javaparser.ast.expr.FieldAccessExpr;
import com.github.javaparser.ast.stmt.ExpressionStmt;
import com.github.javaparser.ast.stmt.IfStmt;
import com.github.javaparser.ast.stmt.ReturnStmt;
import com.github.javaparser.ast.visitor.ModifierVisitor;
import com.github.javaparser.ast.visitor.Visitable;
import de.tkunkel.oss.functional.source.analyzer.AbstractChecker;
import de.tkunkel.oss.functional.source.analyzer.model.FunctionalProblemFinding;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

class VoidModifierVisitor extends ModifierVisitor<Void> {

    private final AbstractChecker checker;
    private final String fileName;
    private final String[] packageName;
    private final List<FunctionalProblemFinding> rc;

    VoidModifierVisitor(String fileName, String[] packageName, List<FunctionalProblemFinding> rc, AbstractChecker checker) {
        this.checker = checker;
        this.fileName = fileName;
        this.packageName = packageName;
        this.rc = rc;
    }

    @Override
    public Visitable visit(final ReturnStmt returnStmt, final Void arg) {
        Optional<Expression> expressionOptional = returnStmt.getExpression();
        if (!expressionOptional.isPresent()) {
            return super.visit(returnStmt, arg);
        }

        List<BinaryExpr> allInnerBinaryExpressions = getAllInnerBinaryExpressions(expressionOptional.get());
        boolean any = false;
        for (BinaryExpr binaryExpr : allInnerBinaryExpressions) {
            any = handleBinaryExpression(binaryExpr, fileName, packageName[0], rc, returnStmt.toString()) || any;
        }
        if (any) {
            return null;
        }
        return super.visit(returnStmt, arg);
    }

    @Override
    public Visitable visit(final ExpressionStmt expressionStmt, final Void arg) {
        List<BinaryExpr> allInnerBinaryExpressions = getAllInnerBinaryExpressions(expressionStmt.getExpression());
        boolean any = false;
        for (BinaryExpr binaryExpr : allInnerBinaryExpressions) {
            any = handleBinaryExpression(binaryExpr, fileName, packageName[0], rc, expressionStmt.toString()) || any;
        }
        if (any) {
            return null;
        }
        return super.visit(expressionStmt, arg);
    }

    @Override
    public Visitable visit(final EnclosedExpr n, final Void arg) {
        if (n.getInner().isBinaryExpr()) {
            BinaryExpr binaryExpr = n.getInner().asBinaryExpr();
            handleBinaryExpression(binaryExpr, fileName, packageName[0], rc, n.toString());
        }
        return super.visit(n, arg);
    }

    @Override
    public Visitable visit(IfStmt n, Void arg) {
        n.getCondition().ifBinaryExpr(binaryExpr -> handleBinaryExpression(binaryExpr, fileName, packageName[0], rc, n.toString()));
        return super.visit(n, arg);
    }

    private List<BinaryExpr> getAllInnerBinaryExpressions(Expression expression) {
        List<BinaryExpr> all = new ArrayList<>();

        if (expression.isBinaryExpr()) {
            all.add(expression.asBinaryExpr());
        }
        expression.getChildNodes().forEach(node -> findBinaryExpressionsRecursive(all, node));
        return all;
    }

    private boolean handleBinaryExpression(BinaryExpr binaryExpr, String fileName, String packageName, List<FunctionalProblemFinding> rc, String code) {
        boolean found;
        if (checkBinaryExpressionComparingEnum(binaryExpr)) {
            Position pos = binaryExpr.getBegin().orElseGet(() -> new Position(-1, -1));
            FunctionalProblemFinding finding = new FunctionalProblemFinding(fileName, packageName, pos.line, pos.column, code);
            rc.add(finding);
            found = true;
        } else {
            found = false;
        }
        return found;
    }

    private void findBinaryExpressionsRecursive(List<BinaryExpr> all, Node node) {
        for (Node child : node.getChildNodes()) {
            findBinaryExpressionsRecursive(all, child);
        }
        if (node instanceof BinaryExpr) {
            all.add((BinaryExpr) node);
        }
    }

    private boolean checkBinaryExpressionComparingEnum(BinaryExpr binaryExpr) {
        final boolean leftIsVariable = isVariable(binaryExpr.getLeft());
        boolean rightIsVariable = isVariable(binaryExpr.getRight());
        final boolean leftIsEnum = checker.getTypeHelper().isEnum(binaryExpr.getLeft());
        final boolean rightIsEnum = checker.getTypeHelper().isEnum(binaryExpr.getRight());
        final boolean bothEnum = leftIsEnum && rightIsEnum;

        final boolean scopeExists = doesScopeExist(binaryExpr.getRight());
        rightIsVariable = rightIsVariable || scopeExists;
        boolean bothVariable = leftIsVariable && rightIsVariable;

        boolean result = false;
        if ((leftIsEnum || rightIsEnum) && !bothEnum && !bothVariable) {
            result = true;
        }

        return result;
    }

    /**
     * Checks if the scope of a given expression can be found, if yes, then it is a variable, if not is is something like an enum.<br>
     * used to differ<br>
     * variable1 == variable2<br>
     * from<br>
     * variable1 == MyEnum.A<br>
     */
    private boolean doesScopeExist(Expression expression) {
        if (!expression.isFieldAccessExpr()) {
            return false;
        }

        boolean hasScope = false;
        try {
            checker.getTypeHelper().getType(((FieldAccessExpr) expression).getScope());
            hasScope = true;
        } catch (Exception e) {
        }
        return hasScope;
    }


    public boolean isVariable(Expression expression) {
        boolean nameExpr = false;
        boolean fieldWithName = false;
        fieldWithName = expression.isFieldAccessExpr() && ((FieldAccessExpr) expression).getScope().isNameExpr();
        nameExpr = expression.isNameExpr();
        return nameExpr;// || fieldWithName;
    }

}