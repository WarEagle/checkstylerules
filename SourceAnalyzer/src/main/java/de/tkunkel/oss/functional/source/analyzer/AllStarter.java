package de.tkunkel.oss.functional.source.analyzer;

import de.tkunkel.oss.functional.source.analyzer.model.FunctionalProblemFinding;
import de.tkunkel.oss.functional.source.analyzer.noenumconditional.NoEnumConditionalChecker;
import de.tkunkel.oss.functional.source.analyzer.noenumequals.NoEnumEqualsChecker;
import de.tkunkel.oss.functional.source.analyzer.switchenumcomplete.SwitchEnumCompleteChecker;
import org.apache.logging.log4j.LogManager;

import java.io.IOException;
import java.nio.file.Path;
import java.util.ArrayDeque;
import java.util.Collection;
import java.util.Deque;
import java.util.function.Predicate;

/**
 * Class to collect all checkers and provide methods to start then in one go.
 */
@SuppressWarnings({"squid:CommentedOutCodeLine"})
public class AllStarter extends ManualStarter {
    private AbstractChecker activeAbstractChecker;

    private AllStarter() {
        logger = LogManager.getLogger(AllStarter.class);
    }

    @Override
    protected AbstractChecker getChecker(String absolutePath, String baseDir) {
        activeAbstractChecker.setProjectDirToScan(absolutePath);
        return activeAbstractChecker;
    }

    /**
     * Executes the scan for all existing checkers
     * @param args path to scan
     * @throws IOException
     * @throws InterruptedException
     */
    public static void main(String[] args) throws IOException, InterruptedException {
        if (args.length < 1) {
            System.err.println("Please provide directory to scan.");
            System.exit(-1);
        }
        String baseDir = args[0];
        AllStarter starter = new AllStarter();

        AbstractChecker noEnumConditionalAbstractChecker = new NoEnumConditionalChecker(baseDir);
        Deque<AbstractChecker> allAbstractChecker = new ArrayDeque<>();
        allAbstractChecker.add(noEnumConditionalAbstractChecker);
        allAbstractChecker.add(new NoEnumEqualsChecker(noEnumConditionalAbstractChecker));
        allAbstractChecker.add(new SwitchEnumCompleteChecker(noEnumConditionalAbstractChecker));

        while (!allAbstractChecker.isEmpty()) {
            starter.activeAbstractChecker = allAbstractChecker.pop();
            starter.logger.info(starter.activeAbstractChecker.getClass());

            Collection<FunctionalProblemFinding> results = starter.startForDirectory(baseDir, getProjectFilter(), getFileFilter());
            starter.outputToLogToConsole(results);
            starter.outputToLogToFile(results);
        }
    }

    @SuppressWarnings({"squid:S1602"})
    private static Predicate<Path> getProjectFilter() {
        return path -> {
            // return path.toString().contains("");
            return true;
        };
    }

    @SuppressWarnings({"squid:S1602"})
    private static Predicate<Path> getFileFilter() {
        return path -> {
            // return path.toString().contains("AbstractCategoryItemRenderer");
            return true;
        };
    }


}
