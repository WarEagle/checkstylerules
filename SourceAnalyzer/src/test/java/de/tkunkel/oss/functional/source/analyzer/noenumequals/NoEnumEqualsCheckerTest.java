package de.tkunkel.oss.functional.source.analyzer.noenumequals;

import de.tkunkel.oss.functional.source.analyzer.CheckerTest;
import de.tkunkel.oss.functional.source.analyzer.model.FunctionalProblemFinding;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.util.List;

class NoEnumEqualsCheckerTest extends CheckerTest {

    private static final String BASIC_DIR = "de/tkunkel/oss/functional/source/analyzer/noenumequals";

    @BeforeEach
    void before() throws IOException {
        abstractChecker = new NoEnumEqualsChecker((String) null);
        abstractChecker.setProjectDirToScan(rootDir);
    }

    @Test
    void simpleMissing() {
        String fileName = "SimpleEquals.java";
        List<FunctionalProblemFinding> result = loadFileAndTest(BASIC_DIR, fileName);
        Assertions.assertEquals(1, result.size());
        Assertions.assertEquals(9, result.get(0).getLineNumber());
        Assertions.assertEquals(22, result.get(0).getColumnNumber());
    }

    @Test
    void normalMethodCall() {
        String fileName = "NormalMethodCall.java";
        List<FunctionalProblemFinding> result = loadFileAndTest(BASIC_DIR, fileName);
        Assertions.assertEquals(0, result.size());
    }

    @Test
    void simpleEqualsDifferentOrder() {
        String fileName = "SimpleEqualsDifferentOrder.java";
        List<FunctionalProblemFinding> result = loadFileAndTest(BASIC_DIR, fileName);
        Assertions.assertEquals(1, result.size());
        Assertions.assertEquals(9, result.get(0).getLineNumber());
        Assertions.assertEquals(22, result.get(0).getColumnNumber());
    }

}
