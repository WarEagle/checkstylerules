package de.tkunkel.oss.functional.source.analyzer.runasjunitexample;

import com.github.javaparser.utils.CodeGenerationUtils;
import de.tkunkel.oss.functional.source.analyzer.AllStarter;
import de.tkunkel.oss.functional.source.analyzer.AbstractChecker;
import de.tkunkel.oss.functional.source.analyzer.ManualStarter;
import de.tkunkel.oss.functional.source.analyzer.model.FunctionalProblemFinding;
import de.tkunkel.oss.functional.source.analyzer.noenumconditional.NoEnumConditionalChecker;
import de.tkunkel.oss.functional.source.analyzer.noenumequals.NoEnumEqualsChecker;
import de.tkunkel.oss.functional.source.analyzer.switchenumcomplete.SwitchEnumCompleteChecker;
import org.apache.logging.log4j.LogManager;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;

import java.io.IOException;
import java.nio.file.Path;
import java.util.ArrayDeque;
import java.util.Collection;
import java.util.Deque;
import java.util.HashMap;
import java.util.Map;
import java.util.function.Predicate;
import java.util.stream.Stream;

@SuppressWarnings({"squid:CommentedOutCodeLine"})
@DisplayName("Check coding guidelines")
public class AllStarterTest extends ManualStarter {
    private static String absoluteBasicDir;

    private AbstractChecker activeAbstractChecker;

    private AllStarterTest() {
        logger = LogManager.getLogger(AllStarter.class);
        absoluteBasicDir = CodeGenerationUtils.classLoaderRoot(this.getClass()).resolve("../src/test/resources/de/tkunkel/oss/functional/source/analyzer/").toString();
    }

    @Override
    protected AbstractChecker getChecker(String absolutePath, String baseDir) {
        activeAbstractChecker.setProjectDirToScan(absolutePath);
        return activeAbstractChecker;
    }

    private static Predicate<Path> getProjectFilter() {
        return path -> {
            // return path.toString().contains("");
            return true;
        };
    }

    private static Predicate<Path> getFileFilter() {
        return path -> {
            // return path.toString().contains("AbstractCategoryItemRenderer");
            return true;
        };
    }

    private static Stream<AbstractChecker> generateCheckers() {
        AbstractChecker noEnumConditionalAbstractChecker;
        Deque<AbstractChecker> allAbstractChecker = new ArrayDeque<>();
        try {
            noEnumConditionalAbstractChecker = new NoEnumConditionalChecker(absoluteBasicDir);
            allAbstractChecker.add(noEnumConditionalAbstractChecker);
            allAbstractChecker.add(new NoEnumEqualsChecker(noEnumConditionalAbstractChecker));
            allAbstractChecker.add(new SwitchEnumCompleteChecker(noEnumConditionalAbstractChecker));
        } catch (IOException e) {
            e.printStackTrace();
        }

        return allAbstractChecker.stream();
    }

    @ParameterizedTest(name = "{0}")
    @MethodSource(value = "generateCheckers")
    @DisplayName("Check specific guideline")
    void checkGuideline(AbstractChecker abstractChecker) throws InterruptedException {
        try {

            Map<String, Integer> testResults = new HashMap<>(3);
            testResults.put(NoEnumConditionalChecker.class.getSimpleName(), 18);
            testResults.put(NoEnumEqualsChecker.class.getSimpleName(), 2);
            testResults.put(SwitchEnumCompleteChecker.class.getSimpleName(), 2);

            AllStarterTest starter = new AllStarterTest();
            starter.activeAbstractChecker = abstractChecker;
            starter.logger.info(starter.activeAbstractChecker.getClass());
            Collection<FunctionalProblemFinding> results = starter.startForDirectory(absoluteBasicDir, getProjectFilter(), getFileFilter());

            int expectedSize = testResults.get(starter.activeAbstractChecker.getClass().getSimpleName());
            Assertions.assertEquals(expectedSize, results.size());

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
