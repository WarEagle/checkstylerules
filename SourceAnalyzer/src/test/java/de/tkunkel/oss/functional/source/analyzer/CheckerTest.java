package de.tkunkel.oss.functional.source.analyzer;

import com.github.javaparser.ast.CompilationUnit;
import com.github.javaparser.utils.CodeGenerationUtils;
import com.github.javaparser.utils.SourceRoot;
import de.tkunkel.oss.functional.source.analyzer.model.FunctionalProblemFinding;
import org.junit.jupiter.api.Assertions;

import java.util.List;

public abstract class CheckerTest {

    protected final String rootDir = "../../src/test/resources";
    protected AbstractChecker abstractChecker;

    protected List<FunctionalProblemFinding> loadFileAndTest(String dir, String fileToTest) {
        SourceRoot root = new SourceRoot(CodeGenerationUtils.classLoaderRoot(this.getClass()).resolve("../../src/test/resources"));

        CompilationUnit cu = root.parse(dir, fileToTest);

        List<FunctionalProblemFinding> result = abstractChecker.checkCompilationUnit(cu, fileToTest);
        Assertions.assertNotNull(result);
        result.forEach(r -> Assertions.assertEquals(fileToTest, r.getFileName()));
        return result;
    }

}
