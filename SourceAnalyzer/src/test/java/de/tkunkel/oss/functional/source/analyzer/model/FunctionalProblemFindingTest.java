package de.tkunkel.oss.functional.source.analyzer.model;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

class FunctionalProblemFindingTest {

    @Test
    void checkEquals() {
        FunctionalProblemFinding a = new FunctionalProblemFinding("file", "package", 10, 20, "myCode");
        FunctionalProblemFinding b = new FunctionalProblemFinding("file", "package", 10, 20, "myCode");
        Assertions.assertEquals(a, b);

        a = new FunctionalProblemFinding("file", "package", 10, 20, "myCode");
        b = new FunctionalProblemFinding("fileChanged", "package", 10, 20, "myCode");
        Assertions.assertNotEquals(a, b);

        a = new FunctionalProblemFinding("file", "package", 10, 20, "myCode");
        b = new FunctionalProblemFinding("file", "packageChanged", 10, 20, "myCode");
        Assertions.assertNotEquals(a, b);

        a = new FunctionalProblemFinding("file", "package", 10, 20, "myCode");
        b = new FunctionalProblemFinding("file", "package", 100, 20, "myCode");
        Assertions.assertNotEquals(a, b);

        a = new FunctionalProblemFinding("file", "package", 10, 20, "myCode");
        b = new FunctionalProblemFinding("file", "package", 10, 200, "myCode");
        Assertions.assertNotEquals(a, b);

        a = new FunctionalProblemFinding("file", "package", 10, 20, "myCode");
        b = new FunctionalProblemFinding("file", "package", 10, 200, "myCodeChanged");
        Assertions.assertNotEquals(a, b);
    }

}