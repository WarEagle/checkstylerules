package de.tkunkel.oss.functional.source.analyzer.noenumconditional;

import de.tkunkel.oss.functional.source.analyzer.CheckerTest;
import de.tkunkel.oss.functional.source.analyzer.model.FunctionalProblemFinding;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.util.List;

class NoEnumConditionalCheckerTest extends CheckerTest {

    private static final String BASIC_DIR = "de/tkunkel/oss/functional/source/analyzer/noenumconditional";

    @BeforeEach
    void before() throws IOException {
        abstractChecker = new NoEnumConditionalChecker((String) null);
        abstractChecker.setProjectDirToScan(rootDir);
    }

    @Test
    void simple() {
        String fileName = "IfSimple.java";
        List<FunctionalProblemFinding> result = loadFileAndTest(BASIC_DIR, fileName);
        Assertions.assertEquals(1, result.size());
        Assertions.assertEquals(9, result.get(0).getLineNumber());
        Assertions.assertEquals(13, result.get(0).getColumnNumber());
    }

    @Test
    void multipleFindings() {
        String fileName = "IfMulti.java";
        List<FunctionalProblemFinding> result = loadFileAndTest(BASIC_DIR, fileName);
        Assertions.assertEquals(4, result.size());
    }

    @Test
    void allOk() {
        String fileName = "IfAllOk.java";
        List<FunctionalProblemFinding> result = loadFileAndTest(BASIC_DIR, fileName);
        Assertions.assertEquals(0, result.size());
    }

    @Test
    void mixed() {
        String fileName = "IfMixed.java";
        List<FunctionalProblemFinding> result = loadFileAndTest(BASIC_DIR, fileName);
        Assertions.assertEquals(1, result.size());
        Assertions.assertEquals(11, result.get(0).getLineNumber());
        Assertions.assertEquals(13, result.get(0).getColumnNumber());
    }

    @Test
    void notCompare() {
        String fileName = "IfNotCompare.java";
        List<FunctionalProblemFinding> result = loadFileAndTest(BASIC_DIR, fileName);
        Assertions.assertEquals(1, result.size());
        Assertions.assertEquals(9, result.get(0).getLineNumber());
        Assertions.assertEquals(13, result.get(0).getColumnNumber());
    }

    @Test
    void split() {
        String fileName = "If_Split_Usage.java";
        List<FunctionalProblemFinding> result = loadFileAndTest(BASIC_DIR, fileName);
        Assertions.assertEquals(1, result.size());
        Assertions.assertEquals(7, result.get(0).getLineNumber());
        Assertions.assertEquals(13, result.get(0).getColumnNumber());
    }

    @Test
    void booleanSimple() {
        String fileName = "BooleanSimple.java";
        List<FunctionalProblemFinding> result = loadFileAndTest(BASIC_DIR, fileName);
        Assertions.assertEquals(1, result.size());
        Assertions.assertEquals(9, result.get(0).getLineNumber());
        Assertions.assertEquals(23, result.get(0).getColumnNumber());
    }

    @Test
    void booleanMulti() {
        String fileName = "BooleanMulti.java";
        List<FunctionalProblemFinding> result = loadFileAndTest(BASIC_DIR, fileName);
        Assertions.assertEquals(2, result.size());
        Assertions.assertEquals(9, result.get(0).getLineNumber());
        Assertions.assertEquals(23, result.get(0).getColumnNumber());
        Assertions.assertEquals(9, result.get(1).getLineNumber());
        Assertions.assertEquals(49, result.get(1).getColumnNumber());
    }

    @Test
    void booleanMultiParenthesis() {
        String fileName = "BooleanMultiParenthesis.java";
        List<FunctionalProblemFinding> result = loadFileAndTest(BASIC_DIR, fileName);
        Assertions.assertEquals(2, result.size());
        Assertions.assertEquals(9, result.get(0).getLineNumber());
        Assertions.assertEquals(24, result.get(0).getColumnNumber());
        Assertions.assertEquals(9, result.get(1).getLineNumber());
        Assertions.assertEquals(50, result.get(1).getColumnNumber());
    }

    @Test
    void booleanMultiNoParenthesis() {
        String fileName = "BooleanMultiNoParenthesis.java";
        List<FunctionalProblemFinding> result = loadFileAndTest(BASIC_DIR, fileName);
        Assertions.assertEquals(2, result.size());
        Assertions.assertEquals(9, result.get(0).getLineNumber());
        Assertions.assertEquals(22, result.get(0).getColumnNumber());
        Assertions.assertEquals(9, result.get(1).getLineNumber());
        Assertions.assertEquals(46, result.get(1).getColumnNumber());
    }

    @Test
    void enumCompareReturn() {
        String fileName = "EnumCompareReturn.java";
        List<FunctionalProblemFinding> result = loadFileAndTest(BASIC_DIR, fileName);
        Assertions.assertEquals(2, result.size());
        Assertions.assertEquals(13, result.get(0).getLineNumber());
        Assertions.assertEquals(16, result.get(0).getColumnNumber());
        Assertions.assertEquals(14, result.get(1).getLineNumber());
        Assertions.assertEquals(17, result.get(1).getColumnNumber());
    }

    @Test
    void enumCompareReturn2() {
        String fileName = "EnumCompareReturn2.java";
        List<FunctionalProblemFinding> result = loadFileAndTest(BASIC_DIR, fileName);
        Assertions.assertEquals(2, result.size());
        Assertions.assertEquals(9, result.get(0).getLineNumber());
        Assertions.assertEquals(16, result.get(0).getColumnNumber());
        Assertions.assertEquals(10, result.get(1).getLineNumber());
        Assertions.assertEquals(17, result.get(1).getColumnNumber());
    }

    @Test
    void castsNoProblems() {
        String fileName = "CastsNoProblems.java";
        List<FunctionalProblemFinding> result = loadFileAndTest(BASIC_DIR, fileName);
        Assertions.assertEquals(0, result.size());
    }

    @Test
    void onlyVariables() {
        String fileName = "OnlyVariables.java";
        List<FunctionalProblemFinding> result = loadFileAndTest(BASIC_DIR, fileName);
        Assertions.assertEquals(0, result.size());
    }

    @Test
    void onlyVariables2() {
        String fileName = "OnlyVariables2.java";
        List<FunctionalProblemFinding> result = loadFileAndTest(BASIC_DIR, fileName);
        Assertions.assertEquals(0, result.size());
    }

}
