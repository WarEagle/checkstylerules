package de.tkunkel.oss.functional.source.analyzer.runasjunitexample;

import com.github.javaparser.utils.CodeGenerationUtils;
import de.tkunkel.oss.functional.source.analyzer.AllStarter;
import de.tkunkel.oss.functional.source.analyzer.AbstractChecker;
import de.tkunkel.oss.functional.source.analyzer.ManualStarter;
import de.tkunkel.oss.functional.source.analyzer.model.FunctionalProblemFinding;
import de.tkunkel.oss.functional.source.analyzer.noenumconditional.NoEnumConditionalChecker;
import de.tkunkel.oss.functional.source.analyzer.noenumequals.NoEnumEqualsChecker;
import de.tkunkel.oss.functional.source.analyzer.switchenumcomplete.SwitchEnumCompleteChecker;
import org.apache.logging.log4j.LogManager;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.nio.file.Path;
import java.util.ArrayDeque;
import java.util.Collection;
import java.util.Deque;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@SuppressWarnings({"squid:CommentedOutCodeLine"})
@DisplayName("Check coding guidelines")
public class AllStarterDemoTest extends ManualStarter {
    private static final String absoluteBasicDir = CodeGenerationUtils.classLoaderRoot(AllStarterDemoTest.class.getClass()).resolve("../../src/test/resources/de/tkunkel/checks/").toString();

    private AbstractChecker activeAbstractChecker;

    private AllStarterDemoTest() {
        logger = LogManager.getLogger(AllStarter.class);
    }

    @Override
    protected AbstractChecker getChecker(String absolutePath, String baseDir) {
        AbstractChecker abstractChecker = null;

        try {
            abstractChecker = activeAbstractChecker.getClass().getDeclaredConstructor(String.class).newInstance(baseDir);
            abstractChecker.setProjectDirToScan(absolutePath);
        } catch (InstantiationException | NoSuchMethodException | InvocationTargetException | IllegalAccessException e) {
            logger.error(e.getMessage(), e);
            Assertions.fail(e);
        }

        return abstractChecker;
    }

    private static Predicate<Path> getProjectFilter() {
        return path -> {
            // return path.toString().contains("");
            return true;
        };
    }

    private static Predicate<Path> getFileFilter() {
        return path -> {
            // return path.toString().contains("AbstractCategoryItemRenderer");
            return true;
        };
    }

    private static Stream<AbstractChecker> generateCheckers() {
        AbstractChecker noEnumConditionalAbstractChecker;
        Deque<AbstractChecker> allAbstractChecker = new ArrayDeque<>();
        try {
            noEnumConditionalAbstractChecker = new NoEnumConditionalChecker(absoluteBasicDir);
            allAbstractChecker.add(noEnumConditionalAbstractChecker);
            allAbstractChecker.add(new NoEnumEqualsChecker(noEnumConditionalAbstractChecker));
            allAbstractChecker.add(new SwitchEnumCompleteChecker(noEnumConditionalAbstractChecker));
        } catch (IOException e) {
            e.printStackTrace();
        }

        return allAbstractChecker.stream();
    }

    @ParameterizedTest(name = "{0}")
    @MethodSource(value = "generateCheckers")
    @DisplayName("Check specific guideline")
    @Disabled("Only for demonstration purpose, is not intended to be run during build")
    void checkGuideline(AbstractChecker abstractChecker) throws InterruptedException {
        try {

            AllStarterDemoTest starter = new AllStarterDemoTest();
            starter.activeAbstractChecker = abstractChecker;
            starter.logger.info(starter.activeAbstractChecker.getClass());
            Collection<FunctionalProblemFinding> results = starter.startForDirectory(absoluteBasicDir, getProjectFilter(), getFileFilter());

            String message = results.stream().map(FunctionalProblemFinding::toDisplayString).collect(Collectors.joining("\n\n")) + '\n';
            Assertions.assertTrue(results.isEmpty(), message);

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
