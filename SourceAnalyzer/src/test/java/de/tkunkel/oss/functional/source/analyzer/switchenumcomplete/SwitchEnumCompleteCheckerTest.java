package de.tkunkel.oss.functional.source.analyzer.switchenumcomplete;

import com.github.javaparser.ast.CompilationUnit;
import com.github.javaparser.ast.stmt.SwitchStmt;
import com.github.javaparser.ast.visitor.ModifierVisitor;
import com.github.javaparser.ast.visitor.Visitable;
import com.github.javaparser.utils.CodeGenerationUtils;
import com.github.javaparser.utils.SourceRoot;
import de.tkunkel.oss.functional.source.analyzer.CheckerTest;
import de.tkunkel.oss.functional.source.analyzer.helper.TypeHelper;
import de.tkunkel.oss.functional.source.analyzer.model.FunctionalProblemFinding;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.util.List;
import java.util.Set;

class SwitchEnumCompleteCheckerTest extends CheckerTest {

    private static final String BASIC_DIR = "de/tkunkel/oss/functional/source/analyzer/switchenumcomplete";

    @BeforeEach
    void before() throws IOException {
        abstractChecker = new SwitchEnumCompleteChecker((String) null);
        abstractChecker.setProjectDirToScan(rootDir);
    }

    @Test
    void simpleMissing() {
        String fileName = "SwitchSimpleMissing.java";
        List<FunctionalProblemFinding> result = loadFileAndTest(BASIC_DIR, fileName);
        Assertions.assertEquals(1, result.size());
        Assertions.assertEquals(9, result.get(0).getLineNumber());
        Assertions.assertEquals(9, result.get(0).getColumnNumber());
    }

    @Test
    void simpleMissing2() {
        String fileName = "SwitchSimpleMissing2.java";
        List<FunctionalProblemFinding> result = loadFileAndTest(BASIC_DIR, fileName);
        Assertions.assertEquals(1, result.size());
        Assertions.assertEquals(9, result.get(0).getLineNumber());
        Assertions.assertEquals(9, result.get(0).getColumnNumber());
    }

    @Test
    void simpleComplete() {
        String fileName = "SwitchSimpleComplete.java";
        List<FunctionalProblemFinding> result = loadFileAndTest(BASIC_DIR, fileName);
        Assertions.assertEquals(0, result.size());
    }

    @Test
    void simpleComplete2() {
        String fileName = "SwitchSimpleComplete2.java";
        List<FunctionalProblemFinding> result = loadFileAndTest(BASIC_DIR, fileName);
        Assertions.assertEquals(0, result.size());
    }

    @Test
    void string() {
        String fileName = "SwitchString.java";
        List<FunctionalProblemFinding> result = loadFileAndTest(BASIC_DIR, fileName);
        Assertions.assertEquals(0, result.size());
    }

    @Test
    void switchThisEnumDetection() {
        String fileName = "SwitchThis.java";
        SourceRoot root = new SourceRoot(CodeGenerationUtils.classLoaderRoot(this.getClass()).resolve("../../src/test/resources"));
        CompilationUnit cu = root.parse(BASIC_DIR, fileName);
        SwitchEnumCompleteChecker switchEnumCompleteChecker = (SwitchEnumCompleteChecker) abstractChecker;

        cu.accept(new ModifierVisitor<Void>() {

            @Override
            public Visitable visit(final SwitchStmt switchStmt, final Void arg) {
                TypeHelper typeHelper = switchEnumCompleteChecker.getTypeHelper();
                Set<String> entriesOfSwitch = typeHelper.getLabelsOfSwitch(switchStmt);
                Assertions.assertEquals(2, entriesOfSwitch.size());
                Set<String> entriesOfEnum = typeHelper.getEntriesOfEnum(switchStmt);
                Assertions.assertEquals(2, entriesOfEnum.size());

                return super.visit(switchStmt, arg);
            }
        }, null);
    }

    @Test
    void switchThis() {
        String fileName = "SwitchThis.java";
        List<FunctionalProblemFinding> result = loadFileAndTest(BASIC_DIR, fileName);
        Assertions.assertEquals(0, result.size());
    }

    @Test
    void switchThisMissing() {
        String fileName = "SwitchThisMissing.java";
        List<FunctionalProblemFinding> result = loadFileAndTest(BASIC_DIR, fileName);
        Assertions.assertEquals(1, result.size());
    }

    @Test
    void switchThisWithPrivateVariable() {
        String fileName = "SwitchThisWithPrivateVariable.java";
        List<FunctionalProblemFinding> result = loadFileAndTest(BASIC_DIR, fileName);
        Assertions.assertEquals(0, result.size());
    }

}
