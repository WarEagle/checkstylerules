package de.tkunkel.oss.functional.source.analyzer.noenumconditional;

import com.github.javaparser.ast.expr.FieldAccessExpr;
import com.github.javaparser.ast.expr.NameExpr;
import com.github.javaparser.ast.expr.SimpleName;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.io.IOException;

class VoidModifierVisitorTest {

    @Test
    void isVariable() throws IOException {
        VoidModifierVisitor voidModifierVisitor = new VoidModifierVisitor(null, null, null, null);
        FieldAccessExpr expression = new FieldAccessExpr();
        expression.setScope(new NameExpr("MyEnum"));
        expression.setName(new SimpleName("A"));

        boolean isVariable = voidModifierVisitor.isVariable(expression);

        Assertions.assertFalse(isVariable);
    }

}