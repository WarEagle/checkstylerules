package de.tkunkel.oss.functional.source.analyzer.noenumequals;

enum MyEnum {
    A, B;
}

public class SimpleEqualsDifferentOrder {
    public boolean test1(MyEnum variable) {
        boolean rc = MyEnum.A.equals(variable);
        return rc;
    }
}
