package de.tkunkel.oss.functional.source.analyzer.noenumconditional;

enum MyEnum {
    A, B;
}

public class EnumCompareReturn {
    public MyEnum getSomething() {
        return MyEnum.A;
    }

    public boolean test1(MyEnum variable) {
        return getSomething() != MyEnum.A &&
                getSomething() != MyEnum.B &&
                getImportStatus() != null;
    }
}
