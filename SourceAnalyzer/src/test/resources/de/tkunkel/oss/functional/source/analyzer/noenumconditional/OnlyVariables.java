package de.tkunkel.oss.functional.source.analyzer.noenumconditional;

enum MyEnum {
    A, B;
}

public class OnlyyVariables {
    public void test1(MyEnum variable1, MyEnum variable2) {
        if (variable1 == variable2) {
        }
    }
}
