package de.tkunkel.oss.functional.source.analyzer.noenumconditional;

enum MyEnum {
    A, B;
}

public class IfMulti {
    public void test1(MyEnum variable) {
        if (variable == MyEnum.A) {
        }
        if (variable == MyEnum.A) {
        }
        if (variable == MyEnum.A) {
        }
        if (variable == MyEnum.A) {
        }
    }
}
