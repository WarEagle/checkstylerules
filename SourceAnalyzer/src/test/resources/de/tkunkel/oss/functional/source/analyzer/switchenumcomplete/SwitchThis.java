package de.tkunkel.oss.functional.source.analyzer.switchenumcomplete;

public enum SwitchThis {
    A,
    B;

    public boolean is() {
        boolean result = false;

        switch (this) {
            case A:
                result = false;
            case B:
                result = false;
                break;
        }
        return result;
    }
}
