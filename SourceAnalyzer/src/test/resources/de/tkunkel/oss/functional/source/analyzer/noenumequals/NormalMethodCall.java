package de.tkunkel.oss.functional.source.analyzer.noenumequals;

enum MyEnum {
    A, B;
}

public class NormalMethodCall {
    public boolean test2(MyEnum variable) {
        return false;
    }

    public boolean test1(MyEnum variable) {
        boolean rc = test2(Myenum.A);
        return rc;
    }
}
