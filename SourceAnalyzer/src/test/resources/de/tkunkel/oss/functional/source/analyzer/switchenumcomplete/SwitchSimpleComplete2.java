package de.tkunkel.oss.functional.source.analyzer.switchenumcomplete;

enum MyEnum {
    A, B, DEFAULT;
}

public class SwitchSimpleComplete2 {
    public void test1(MyEnum variable) {
        switch (variable) {
            case A:
                System.out.println("A");
                break;
            case B:
                System.out.println("B");
                break;
            case DEFAULT:
                System.out.println("DEFAULT");
                break;
            default:
                System.out.println("None");
        }
    }
}
