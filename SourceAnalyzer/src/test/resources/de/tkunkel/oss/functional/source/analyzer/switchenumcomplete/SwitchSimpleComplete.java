package de.tkunkel.oss.functional.source.analyzer.switchenumcomplete;

enum MyEnum {
    A, B;
}

public class SwitchSimpleComplete {
    public void test1(MyEnum variable) {
        switch (variable) {
            case A:
                System.out.println("A");
                break;
            case B:
                System.out.println("B");
                break;
            default:
                System.out.println("None");
        }
    }
}
