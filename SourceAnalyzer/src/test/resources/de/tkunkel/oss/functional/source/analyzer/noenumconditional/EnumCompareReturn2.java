package de.tkunkel.oss.functional.source.analyzer.noenumconditional;

enum MyEnum {
    A, B;
}

public class EnumCompareReturn2 {
    public boolean test1(MyEnum variable) {
        return variable != MyEnum.A &&
                variable != MyEnum.B &&
                variable != null;
    }
}
