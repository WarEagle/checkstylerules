package de.tkunkel.oss.functional.source.analyzer.noenumconditional;

public class If_Split_Usage {
    public void test1(MyEnum variable) {
        if (variable == variable) {
        }
        if (variable == MyEnum.A) {
        }
        if (variable == A) {
        }
        if (variable == "A") {
        }
    }
}
