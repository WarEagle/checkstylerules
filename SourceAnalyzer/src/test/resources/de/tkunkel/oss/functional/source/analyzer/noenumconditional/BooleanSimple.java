package de.tkunkel.oss.functional.source.analyzer.noenumconditional;

enum MyEnum {
    A, B
}

public class BooleanSimple {
    public void test1(MyEnum variable) {
        boolean rc = (variable == MyEnum.A);
    }
}
