package de.tkunkel.oss.functional.source.analyzer.switchenumcomplete;

enum MyEnum {
    A, B, C;
}

public class SwitchSimpleMissing2 {
    public void test1(MyEnum variable) {
        switch (variable) {
            case MyEnum.A:
                System.out.println("A");
                break;
            case B:
                System.out.println("B");
                break;
        }
    }
}
