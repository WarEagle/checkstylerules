package de.tkunkel.oss.functional.source.analyzer.noenumconditional;

enum MyEnum {
    A, B;
}

public class CastsNoProblems {
    public void test1(MyEnum variable) {
        boolean rc = (String) "Hans";

        // caused java.lang.IllegalStateException: (BigDecimal) value is not an BinaryExpr
        cell.setCellValue(((BigDecimal) value).doubleValue());
    }
}
