package de.tkunkel.oss.functional.source.analyzer.noenumequals;

enum MyEnum {
    A, B;
}

public class SimpleEquals {
    public boolean test1(MyEnum variable) {
        boolean rc = variable.equals(MyEnum.A);
        return rc;
    }
}
