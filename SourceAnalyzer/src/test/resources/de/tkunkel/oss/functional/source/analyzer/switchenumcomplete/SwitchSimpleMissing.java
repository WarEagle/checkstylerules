package de.tkunkel.oss.functional.source.analyzer.switchenumcomplete;

enum MyEnum {
    A, B;
}

public class SwitchSimpleMissing {
    public void test1(MyEnum variable) {
        switch (variable) {
            case A:
                System.out.println("A");
                break;
            default:
                System.out.println("None");
        }
    }
}
