package de.tkunkel.oss.functional.source.analyzer.noenumconditional;

enum MyEnum {
    A, B;
}

public class OnlyVariables2 {

    private MyEnum type;

    public OnlyVariables2(MyEnum type) {
        this.type = type;
    }

    public void setType(MyEnum type) {
        this.type = type;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        OnlyVariables2 metric = (OnlyVariables2) o;

        return type == metric.type;
    }

}
