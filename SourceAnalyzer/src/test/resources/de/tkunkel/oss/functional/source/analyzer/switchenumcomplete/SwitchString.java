package de.tkunkel.oss.functional.source.analyzer.switchenumcomplete;

public class SwitchString {
    public void test1(String variable) {
        switch (variable) {
            case "A":
                System.out.println("A");
                break;
            case "B":
                System.out.println("B");
                break;
            default:
                System.out.println("None");
        }
    }
}
