package de.tkunkel.oss.functional.source.analyzer.noenumconditional;

enum MyEnum {
    A, B;
}

public class IfAllOk {
    public void test1(MyEnum variable) {
        if (variable == variable) {
        }
        if (variable == "MyEnum.A") {
        }
        if (variable == A) {
        }
        if (variable == "A") {
        }
    }
}
