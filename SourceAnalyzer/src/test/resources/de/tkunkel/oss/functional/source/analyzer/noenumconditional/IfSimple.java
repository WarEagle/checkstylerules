package de.tkunkel.oss.functional.source.analyzer.noenumconditional;

enum MyEnum {
    A, B;
}

public class IfSimple {
    public void test1(MyEnum variable) {
        if (variable == MyEnum.A) {
        }
    }
}
