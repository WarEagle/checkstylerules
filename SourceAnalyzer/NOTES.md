### Make release
#### Short:
```
change version in pom.xml to without snapshot
mvn deploy -f SourceAnalyzer\pom.xml
mvn nexus-staging:release -f SourceAnalyzer\pom.xml
change version in pom.xml to with snapshot
```

#### 1. Clean up after a release preparation.
```
mvn release:clean -f SourceAnalyzer\pom.xml
```

#### 2. Prepare for a release in SCM
```
mvn release:prepare -DpushChanges=false -f SourceAnalyzer\pom.xml
```

#### 3. make release
```
mvn deploy -f SourceAnalyzer\pom.xml
mvn nexus-staging:release -f SourceAnalyzer\pom.xml
```

Errors:
```
release:rollback
git tag --delete <NAMEOFTAG>
mvn nexus-staging:drop -f SourceAnalyzer\pom.xml
```
